#include "TFT_eSPI.h"

int X_MAX = 320;
int Y_MAX = 240;
TFT_eSPI tft;
int visit_total = 0; // Initialize visit_total to 0
int affluence = 0;   // Initialize affluence to 0
int ancien_visit_total = 0;
int ancien_affluence = 0;

void setup() {
    tft.begin();
    tft.setRotation(3); // Set the screen orientation
    tft.fillScreen(TFT_GREEN); // Green background
    pinMode(WIO_KEY_A, INPUT_PULLUP);
    pinMode(WIO_KEY_B, INPUT_PULLUP);
    pinMode(WIO_KEY_C, INPUT_PULLUP);
    pinMode(WIO_5S_PRESS, INPUT_PULLUP);
    affiche();
}

void loop() {
    visiteur();
}

void visiteur() {
    int etat = 0;
    if (digitalRead(WIO_KEY_B) == LOW) {
        ancien_affluence = affluence;
        affluence = affluence -1; // Decrease affluence
        if(affluence < 0){
          Serial.println("impossible d'être négatif !");
          affluence = 0;
        }
        efface();
        affiche();
        Serial.print("visit_total = ");
        Serial.println(visit_total);
        Serial.print("affluence = ");
        Serial.println(affluence);
    } else if (digitalRead(WIO_KEY_C) == LOW) {
        ancien();
        affluence = affluence +1; // Increase affluence
        visit_total = visit_total +1; // Increment visit_total
        if(affluence > 10){
          Serial.println("attention l'affluence à dépassé 10 !");
        }
        efface();
        affiche();
        Serial.print("visit_total = ");
        Serial.println(visit_total);
        Serial.print("affluence = ");
        Serial.println(affluence);
    } else if (digitalRead(WIO_KEY_A) == LOW) {
        tft.setTextColor(TFT_BLACK);
        tft.setTextSize(3);
        tft.drawString("Confirmez ?", 150, 200);
        etat = compteur();
        if(etat == 1){
          ancien();
          visit_total = 0;
          affluence = 0;
          etat = 0;
          
        }
        tft.setTextColor(TFT_GREEN);
        tft.setTextSize(3);
        tft.drawString("Confirmez ?", 130, 200);
        efface();
        affiche();
        Serial.print("visit_total = ");
        Serial.println(visit_total);
        Serial.print("affluence = ");
        Serial.println(affluence);
    }
    delay(200);
}

void ancien(){
  ancien_visit_total = visit_total;
  ancien_affluence = affluence;
}
void efface(){
    tft.setTextColor(TFT_GREEN);
    tft.setTextSize(3);
    tft.drawString("visit_total", 10, 100);
    tft.drawNumber(ancien_visit_total, 250, 100);
    tft.drawString("affluence", 10, 150);
    tft.drawNumber(ancien_affluence, 250, 150);
}

void affiche(){
    tft.setTextColor(TFT_BLACK);
    tft.setTextSize(3);
    tft.drawString("visit_total", 10, 100);
    tft.drawNumber(visit_total, 250, 100);
    tft.drawString("affluence", 10, 150);
    tft.drawNumber(affluence, 250, 150);
}




int compteur(){
  int i = 5;
  while( i > 0){
    delay(1000);
    Serial.println(i);
    i--;
    if(digitalRead(WIO_5S_PRESS) == LOW){
      return 1;
    }
  }
  return 0;
}
